/** @type {import('@sveltejs/kit').ParamMatcher} */
// TODO: Enhance this to do something like game-id or something
export function match(param) {
  return /^\d+$/.test(param);
}
