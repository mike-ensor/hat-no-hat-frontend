import {INITIALIZED, setInitialized} from '$lib/constants';
import {appEnvs} from '$lib/appEnvs.js';

/** @type {import('@sveltejs/kit').Handle} */
export async function handle({event, resolve}) {
    if (!INITIALIZED) {
        console.log("Startup sequence initiated.");
        console.log("===========================");
        console.log("RTSP_HOST: http://" + appEnvs.RTSP_HOST + ":" + appEnvs.RTSP_PORT);
        console.log("INFERENCE_HOST: http://" + appEnvs.INFERENCE_HOST + ":" + appEnvs.INFERENCE_PORT);
        console.log("LIGHT_TOWER_HOST: http://" + appEnvs.LIGHT_TOWER_HOST + ":" + appEnvs.LIGHT_TOWER_PORT);
        console.log("===========================");
        setInitialized();
    }

    return await resolve(event);
}