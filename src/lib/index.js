import {appEnvs} from '$lib/appEnvs.js';

/**
 * Get the list of labels from the metadata and assign a color index
 * @returns {Map<string, Object>}
 */
export function getLabelConfig() {
    const url = `http://${appEnvs.INFERENCE_HOST}:${appEnvs.INFERENCE_PORT}/metadata`;
    const result = fetch(url);

    let labelList = new Map();

    result.then((response) => {
        response.json().then((metadataResponse) => {
            // Convert GameItems to a hashmap of label-based keys
            let i = 0;

            metadataResponse.labels.forEach((label) => {
                labelList.set(label, {
                    color_index: i++
                });
            });

        });
    });

    return labelList;
}
