import {appEnvs} from "$lib/appEnvs.js";

const host = appEnvs.INFERENCE_HOST;
const port = appEnvs.INFERENCE_PORT;
const baseURL = `http://${host}:${port}`;
const inferenceServerURL = `${baseURL}/inference?resize=True`;
const metadataURL = `${baseURL}/metadata`;

export const inferenceFrame = async (imageData) => {
    try {
        const payload = {'image': imageData};

        const inf_result = await fetch(inferenceServerURL, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(payload),
        });

        return await inf_result.json();
    } catch (error) {
        if (error.name === 'TypeError') {
            console.error('Inference failed:', error);
        } else {
            console.error('Error inference data:', error);
        }

        return false;
    }
}

export const inferenceServerMetadata = async () => {
    try {
        const res = await fetch(metadataURL);
        return await res.json();
    } catch (error) {
        if (error instanceof SyntaxError) {
            console.log('There was a JSON SyntaxError during response from Inference Server', error);
        } else {
            console.log('There was an error fetching Inference Server');
            console.debug(error);
        }
    }

    return {};
}