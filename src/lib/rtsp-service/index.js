import {appEnvs} from "$lib/appEnvs.js";


export const getStreamStats = async () => {
    const url = `http://${appEnvs.RTSP_HOST}:${appEnvs.RTSP_PORT}/status`;
    const res = await fetch(url);

    return await res.json();
}

export function getConnectionURL() {
    const rtspHost = appEnvs.RTSP_HOST;
    const rtspPort = appEnvs.RTSP_PORT;
    return "http://" + rtspHost + ":" + rtspPort + "/stream";
}

export const getCurrentRtspUrl = async () => {
    const url = `http://${appEnvs.RTSP_HOST}:${appEnvs.RTSP_PORT}/get-url`;
    const res = await fetch(url);
    return await res.json();
}

export const startStream = async () => {
    const url = `http://${appEnvs.RTSP_HOST}:${appEnvs.RTSP_PORT}/start`;
    const res = await fetch(url);
    const rtspStatus = await res.json();
    console.log(rtspStatus);
    return rtspStatus.state;
}

export const stopStream = async () => {
    const url = `http://${appEnvs.RTSP_HOST}:${appEnvs.RTSP_PORT}/stop`;
    let rtspStatus;

    try {
        const res = await fetch(url);
        rtspStatus = await res.json();
    } catch (error) {
        if (error instanceof SyntaxError) {
            console.log('There was a JSON SyntaxError during response from Inference Server', error);
        } else {
            console.log('There was an error fetching Inference Server');
            console.debug(error);
        }
    }

    if (rtspStatus) {
        console.log(rtspStatus);
        return rtspStatus.state;
    } else {
        console.log("No Valid RTSP Status Response");
        return false;
    }
}

export const setNewRtspUrl = async (newUrl) => {
    const url = `http://${appEnvs.RTSP_HOST}:${appEnvs.RTSP_PORT}/set-url?url=${newUrl}`;
    const res = await fetch(url);
    return await res.json();
}