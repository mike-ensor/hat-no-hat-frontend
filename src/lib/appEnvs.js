import {env} from '$env/dynamic/public';

export const appEnvs = {
    RTSP_HOST: (env.PUBLIC_RTSP_API_HOST ? env.PUBLIC_RTSP_API_HOST : process.env.PUBLIC_RTSP_API_HOST),
    RTSP_PORT: (env.PUBLIC_RTSP_API_PORT ? env.PUBLIC_RTSP_API_PORT : process.env.PUBLIC_RTSP_API_PORT),
    RTSP_NERD_STATS: (env.PUBLIC_RTSP_NERD_STATS ? env.PUBLIC_RTSP_NERD_STATS : process.env.PUBLIC_RTSP_NERD_STATS),
    INFERENCE_HOST: (env.PUBLIC_INFERENCE_API_HOST ? env.PUBLIC_INFERENCE_API_HOST : process.env.PUBLIC_INFERENCE_API_HOST),
    INFERENCE_PORT: (env.PUBLIC_INFERENCE_API_PORT ? env.PUBLIC_INFERENCE_API_PORT : process.env.PUBLIC_INFERENCE_API_PORT),
    LIGHT_TOWER_HOST: (env.PUBLIC_LIGHT_TOWER_HOST ? env.PUBLIC_LIGHT_TOWER_HOST : process.env.PUBLIC_LIGHT_TOWER_HOST),
    LIGHT_TOWER_PORT: (env.PUBLIC_LIGHT_TOWER_PORT ? env.PUBLIC_LIGHT_TOWER_PORT : process.env.PUBLIC_LIGHT_TOWER_PORT),
    APP_VERSION: (env.PUBLIC_APP_VERSION ? env.PUBLIC_APP_VERSION : process.env.PUBLIC_APP_VERSION)
}