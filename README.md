# Overview

This is a demonstration of Edge Object Detection for a trained set of objects in real-time. The goal of the demo is
to identify if a hat is in the field of view, and which hat is being used. This is a front-end project that will facilitate
the UI, provide the feed, inference against frames and draw bounding boxes.

## Development

### Running

```shell
npm run dev
```

## Series Links

This project is one component in a multi-component solution. The composed parts make up the game "Price a Tray".

https://gitlab.com/mike-ensor/price-a-tray-inference-server
: This is the Inference Server that loads a tflite model based on configuration. Inferences are returned as BoundingBoxes data only (ie: no image returned)

https://gitlab.com/mike-ensor/rtsp-to-mjpeg.git
: This project turns a RTSP stream into a MJPEG stream for consumption by the UI
