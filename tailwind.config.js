/** @type {import('tailwindcss').Config} */
export default {
    darkMode: "class",
    content: ["./src/**/*.{html,js,svelte,ts}", './node_modules/flowbite-svelte/**/*.{html,js,svelte,ts}'],
    theme: {
        fontFamily: {
            sans: ['potta', 'sans-serif'],
            serif: ['press-start', 'serif'],
        },
        extend: {
            fontFamily: {
                poppins: ["Poppins", "sans-serif"],
                'eight-bit': ["eight-bit", "monospace"],
                potta: ["potta-one", "poppins"],
                'press-start': ["press-start", "poppins"]
            },
            colors: {
                textColor: "#080f10",
                bgColor: "#f6fafa",
                /*primary: "#5eacae",*/
                // flowbite-svelte
                primary: {
                    50: '#FFF5F2',
                    100: '#FFF1EE',
                    200: '#FFE4DE',
                    300: '#FFD5CC',
                    400: '#FFBCAD',
                    500: '#FE795D',
                    600: '#EF562F',
                    700: '#EB4F27',
                    800: '#CC4522',
                    900: '#A5371B'
                },
                secondary: "#a9b5d4",
                accentColor: "#787fbb",
            },
        },
    },
    plugins: [
        require("@tailwindcss/forms"),
        require("@tailwindcss/typography"),
        require('flowbite/plugin')
    ],
};
